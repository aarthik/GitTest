package pages_bankbazaar;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;

import pages.MyLeadsPage;
import wdMethods.ProjectMethods;

public class bankbazaar_firstpage extends ProjectMethods{

	
		public  bankbazaar_firstpage mousehoverInvestment() {
		builder=new Actions(driver);
		WebElement hover = locateElement("linktext", "INVESTMENTS");
		builder.moveToElement(hover).perform();
		return this;
		}
		
		public bankbazaar_mutulafundspage clickMutualFunds() {
			WebElement clickMutualFunds = locateElement("linktext", "Mutual Funds");
			click(clickMutualFunds);
			return new bankbazaar_mutulafundspage();
			
		}
		
	
		
		
		
		
		
		
		

}
