package pages_bankbazaar;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;

import pages.Merge_FindLeadsPage;
import pages.MyLeadsPage;
import wdMethods.ProjectMethods;

public class bankbazaar_SchemePage extends ProjectMethods{
			
		public bankbazaar_SchemePage getScehemeNames() throws InterruptedException {
			Thread.sleep(3000);
			List<WebElement> schemenames = driver.findElementsByXPath("//li[@class='js-offer-name']/span[@class='js-title']");
			for (WebElement name : schemenames) {
				System.out.println(name.getText());
			}
				List<WebElement> investedAmount = driver.findElementsByXPath("//div[@class='offer-section-column col-same-height col-middle investment-amount']/span[@class='js-title']");
				for (WebElement amount : investedAmount) {
					System.out.println(amount.getText());
					
				}
				/*WebDriverWait wait=new WebDriverWait(driver,15);				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Close']"))).click();
				*/
							
			
			return this;
			
		}
		
		/*public bankbazaar_SchemePage clickAge() throws InterruptedException {
			Thread.sleep(2000);
			WebElement Age = locateElement("xpath", "//div[@class='rangeslider__handle']");
			builder.dragAndDropBy(Age,85,0).perform();
			return this;
			
		}
		
		public bankbazaar_SchemePage clickMonthAndYear() throws InterruptedException {
			Thread.sleep(2000);
			WebElement monthAndYear = locateElement("linktext", "Dec 1988");
			click(monthAndYear);
			return this;
			
		}
		
		public bankbazaar_SchemePage clickDate() throws InterruptedException {
			Thread.sleep(2000);
			WebElement Date = locateElement("xpath", "(//div[text()='27'])[2]");
			click(Date);
			return this;
			
		}
		
		
		public bankbazaar_SchemePage verifyBirthday() throws InterruptedException {
			Thread.sleep(2000);
			WebElement birthday = locateElement("xpath", "//div[@class='Calendar_tableHeading_1ny8Y Calendar_yearLabel_3-jJc']");
			String text = birthday.getText();
			System.out.println(text);
			return this;
			
		}
		
		public bankbazaar_SchemePage clickContinue() {
			WebElement continuebutton = locateElement("linktext", "Continue");
			click(continuebutton);
			return this;
			
		}

		public bankbazaar_SchemePage clickSalary() throws InterruptedException {
			Thread.sleep(2000);
			WebElement Salary = locateElement("xpath", "//div[@class='rangeslider__handle']");
			builder.dragAndDropBy(Salary,181,0).perform();
			return this;
			
		}
		
		public bankbazaar_SchemePage clickBank() {
			WebElement clickHDFC = locateElement("xpath", "//span[text()='HDFC']/following::input[1]");
			click(clickHDFC);
			return this;
			
		}
		
		public bankbazaar_SchemePage typeFirstName(String data) {
			WebElement eleFirstName = locateElement("name", "firstName");
			type(eleFirstName,data);
			return this;
		}
		
		public bankbazaar_SchemePage clickViewMutualFunds() throws InterruptedException {
			Thread.sleep(2000);
			WebElement clickViewMutualFunds = locateElement("linktext", "View Mutual Funds");
			click(clickViewMutualFunds);
			return this;
			
		}
*/}
