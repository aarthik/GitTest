package pages_bankbazaar;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeMethod;

import pages.Merge_FindLeadsPage;
import pages.MyLeadsPage;
import wdMethods.ProjectMethods;

public class bankbazaar_mutulafundspage extends ProjectMethods{
			
		public bankbazaar_mutulafundspage clickSearchforMutualFunds() throws InterruptedException {
			Thread.sleep(3000);
			WebElement searchforMutualFund = locateElement("linktext", "Search for Mutual Funds");
			click(searchforMutualFund);
			return this;
			
		}
		
		public bankbazaar_mutulafundspage clickAge() throws InterruptedException {
			Thread.sleep(2000);
			WebElement Age = locateElement("xpath", "//div[@class='rangeslider__handle']");
			builder.dragAndDropBy(Age,85,0).perform();
			return this;
			
		}
		
		public bankbazaar_mutulafundspage clickMonthAndYear() throws InterruptedException {
			Thread.sleep(2000);
			WebElement monthAndYear = locateElement("linktext", "Dec 1988");
			click(monthAndYear);
			return this;
			
		}
		
		public bankbazaar_mutulafundspage clickDate() throws InterruptedException {
			Thread.sleep(2000);
			WebElement Date = locateElement("xpath", "(//div[text()='27'])[2]");
			click(Date);
			return this;
			
		}
		
		
		public bankbazaar_mutulafundspage verifyBirthday() throws InterruptedException {
			Thread.sleep(2000);
			WebElement birthday = locateElement("xpath", "//div[@class='Calendar_tableHeading_1ny8Y Calendar_yearLabel_3-jJc']");
			String text = birthday.getText();
			System.out.println(text);
			return this;
			
		}
		
		public bankbazaar_mutulafundspage clickContinue() {
			WebElement continuebutton = locateElement("linktext", "Continue");
			click(continuebutton);
			return this;
			
		}

		public bankbazaar_mutulafundspage clickSalary() throws InterruptedException {
			Thread.sleep(2000);
			WebElement Salary = locateElement("xpath", "//div[@class='rangeslider__handle']");
			builder.dragAndDropBy(Salary,181,0).perform();
			return this;
			
		}
		
		public bankbazaar_mutulafundspage clickBank() throws InterruptedException {
			Thread.sleep(2000);
			WebElement clickHDFC = locateElement("xpath", "//span[text()='HDFC']/following::input[1]");
			click(clickHDFC);
			return this;
			
		}
		
		public bankbazaar_mutulafundspage typeFirstName(String data) throws InterruptedException {
			Thread.sleep(2000);
			WebElement eleFirstName = locateElement("name", "firstName");
			type(eleFirstName,data);
			return this;
		}
		
		public bankbazaar_SchemePage clickViewMutualFunds() throws InterruptedException {
			Thread.sleep(3000);
			WebElement clickViewMutualFunds = locateElement("linktext", "View Mutual Funds");
			click(clickViewMutualFunds);
			return new bankbazaar_SchemePage();
			
		}
}
