package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import wdMethods.ProjectMethods;

public class LogInPage extends ProjectMethods{
	
	public LogInPage() {
		PageFactory.initElements(driver, this);
	}
	
	/*@CacheLookup
	@FindBy(id="username")
	WebElement eleUserName;
	
	@CacheLookup
	@FindAll({@FindBy(id="password"),
		  @FindBy(name="PASSWORD")})
	WebElement elePassword;
	
	@CacheLookup
	@FindBy(className="decorativeSubmit")
	WebElement eleLogin;
*/
	@Given("Enter the username as (.*)")
	public LogInPage typeUserName(String data) {
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, data);
		return this;
	}
	
	@And("Enter the password as (.*)")
	public LogInPage typePassword(String data) {
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, data);
		return this;
	}
	
	@And("click on Login button")
	public HomePage clickLogin() throws InterruptedException {
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		return new HomePage();
	}
	
}









