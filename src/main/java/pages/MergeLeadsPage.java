package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	//public String Firstrecord;
	
	public MergeLeadsPage clickFromLeadImage() {
		WebElement eleFromLeadImage = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(eleFromLeadImage);
		return this;
			}
	
	public MergeLeadsPage clickToLeadImage() {
		WebElement eleToLeadImage = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(eleToLeadImage);
		return this;
			}
	
	public Merge_FindLeadsPage switchWindow(int index) {
		switchToWindow(index);
		return new Merge_FindLeadsPage();
			}
	
		
//		public MergeLeadsPage typeLeadIDMerge(String LeadID) {
//			WebElement eleLeadID = locateElement("name", "id");
//			type(eleLeadID,LeadID);
//			return this;
//		}
		
	
	
	public ViewLeadPage clickMerge() {
		WebElement eleMerge = locateElement("linktext", "Merge");
		click(eleMerge);
		acceptAlert();
		return new ViewLeadPage();
			}
		
	
		public MergeLeadsPage clickFindLeads() throws InterruptedException {
			WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
			click(eleFindLeads);
			Thread.sleep(2000);
			/*WebElement message = locateElement("xpath","//div[@class='x-paging-info']");
			String eleVerify = message.getText();*/
			return this;
		}	
		public ViewLeadPage clickFirstLead() {
			WebElement eleFirstLead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		String Firstrecord = eleFirstLead.getText();
			click(eleFirstLead);
			return new ViewLeadPage() ;
		}	
	
}









