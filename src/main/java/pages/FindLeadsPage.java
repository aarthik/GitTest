package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{

	//public String Firstrecord;
	
	public FindLeadsPage clickPhone() {
		WebElement elePhone = locateElement("xpath", "//span[text()='Phone']");
		click(elePhone);
		return this;
	}
		
		public FindLeadsPage typePhonenumber(String data) {
			WebElement elePhonenumber = locateElement("name", "phoneNumber");
			type(elePhonenumber,data);
			return this;
		}
		
		public FindLeadsPage typeFirstName(String data) {
			WebElement eleFirstName = locateElement("name", "firstName");
			type(eleFirstName,data);
			return this;
		}
		
		public FindLeadsPage typeLeadID(String Firstrecord) {
			WebElement eleLeadID = locateElement("name", "id");
			type(eleLeadID,Firstrecord);
			return this;
		}
		public FindLeadsPage verifyErrorMsg(String errorMsg) {
			WebElement eleVerify = locateElement("class", "x-paging-info");
			verifyExactText(eleVerify, errorMsg);
			return this;
		}
		
		
		public FindLeadsPage clickEmail() {
			WebElement eleEmail = locateElement("xpath", "//span[text()='Email']");
			click(eleEmail);
			return this;
		}
		
		public FindLeadsPage typeEmailAddress(String email) {
			WebElement eleEmailAddress = locateElement("name", "emailAddress");
			type(eleEmailAddress,email);
			return this;
		}
		public FindLeadsPage clickFindLeads() throws InterruptedException {
			WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
			click(eleFindLeads);
			Thread.sleep(2000);
			/*WebElement message = locateElement("xpath","//div[@class='x-paging-info']");
			String eleVerify = message.getText();*/
			return this;
		}	
		public ViewLeadPage clickFirstLead() {
			WebElement eleFirstLead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		String Firstrecord = eleFirstLead.getText();
			click(eleFirstLead);
			return new ViewLeadPage() ;
		}	
	
		
		public FindLeadsPage GetFirstLeadname() {
			WebElement eleFirstLeadName = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]");
		 FirstLeadName = eleFirstLeadName.getText();
		System.out.println(FirstLeadName);
				return this;
		}	
		
		public FindLeadsPage VerifyMessage(String errorMsg) {
			WebElement eleVerify = locateElement("class", "x-paging-info");
			verifyExactText(eleVerify, errorMsg);
				return this;
		}
		
	
}









