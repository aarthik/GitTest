/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class createLeadSteps {
	
		public static ChromeDriver driver;
		
	@Given("Login to Leaftaps application")
		public void login() {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.get("http://leaftaps.com/opentaps");
			driver.findElementById("username").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
		}
	
		@And("click on crmsfa link")
		public void clickCrmsfa()
		{
			driver.findElementByLinkText("CRM/SFA").click();
		}
		
		@And("click on Create Lead")
		public void clickCreateLead()
		{
			driver.findElementByLinkText("Create Lead").click();
				}
		
		@And("enter the company name as (.*)")
		public void enterCompanyName(String cName)
		{
			driver.findElementById("createLeadForm_companyName").sendKeys(cName);
		}
		
		@And("enter the first name as (.*)")
		public void enterFirstName(String fName)
		{
			driver.findElementById("createLeadForm_firstName").sendKeys(fName);
		}
		
		@And("enter the last name as (.*)")
		public void enterLastName(String lName)
		{
			driver.findElementById("createLeadForm_lastName").sendKeys(lName);
		}
		
		@When("onClickingCreate Lead button")
		public void clickCreateLeadbutton()
		{
			driver.findElementByClassName("smallSubmit").click();;
		}
		
		@Then("Verify whether Lead is created")
		public void VerifyCreateLead()
		{
			System.out.println("Lead is created successfully");
		}
		
	}
		*/